# Simulate reservations for a parking lot

The parking lot consists of 5 spots where each can be occupied for 24 hours max.

Unreserved hours for a spot are represented by `_` and reserved ones by `|`.

The program creates random reservations every 5".
A reservation is described by:

 - client id (integer)
 - starting hour (integer 0-23)
 - ending hour (integer 1-23)

The program prints every second:

 - the parking lot
 - the reservations
 - the rejected reservations (requested time slot is already reserved or overlaps)

### How to run
```sh
$ python3 app.py
```


### Pass custom reservation
Echo the `<CLIENT_ID`, `<START_HOURT>` and `<END_HOUR>` seperated by space to the pipe that the program creates.
No validation was implemented so please pass valid values. Example:
```sh
$ echo "33 14 23" > /tmp/mypipe
```


### Improvement ideas
On each *update* (added/deleted reservation), sort parking spots so the most reserved spot gets picked up first. This will result in filling the hours of the most reserved spot first, leaving the rest with the possibility for longer reservetions periods.
