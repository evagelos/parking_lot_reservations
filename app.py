import os
import queue
from threading import Thread
from collections import deque
from time import sleep
from random import randint


SPOTS = [['_' for _ in range(24)] for _ in range(5)]
RESERVATIONS = {}
REJECTED_REQUESTS = deque(maxlen=5)
REQUESTS = queue.Queue()
SHUTDOWN = False
THREADS = []


class COLORS:
    HEADER = '\033[95m'
    OKGREEN = '\033[92m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'


def print_parking():
    """Print the parking every second."""
    while not SHUTDOWN:
        os.system('clear')
        for spot in SPOTS:
            line = [
                COLORS.FAIL + str(num) if num else COLORS.OKGREEN + str(num)
                for num in spot
            ]
            print(' '.join(line))

        print(COLORS.HEADER + "Reservations" + COLORS.ENDC)
        for entry in RESERVATIONS.keys():
            print("{}:       {} - {}".format(*entry))

        print(COLORS.HEADER + "Unavailable" + COLORS.ENDC)
        for entry in REJECTED_REQUESTS:
            print("{}: {} - {}".format(*entry))

        sleep(1)


def is_spot_available(spot, start, end):
    """Return if there is a parking spot available given a time slot."""
    return spot[start:end] == ['_'] * (end - start)


def get_available_spot_id(start, end):
    """Return the first available parking spot id given a time slot or None."""
    for idx, spot in enumerate(SPOTS):
        if is_spot_available(spot, start, end):
            return idx
    return None


def add_reservation(spot_id, client_id, start, end):
    """Make a reservation."""
    SPOTS[spot_id][start:end] = ['|'] * (end - start)
    RESERVATIONS[(client_id, start, end)] = spot_id


def remove_reservation(client_id, start, end):
    """Remove a reservation."""
    spot_id = RESERVATIONS.pop((client_id, start, end))
    SPOTS[spot_id][start:end] = ['_'] * (end - start)


def park_car(spot_id, client_id, start, end):
    """Add reservation, sleep while car is parked, remove reservation."""
    add_reservation(spot_id, client_id, start, end)
    parked_time = 0
    while parked_time < 120:
        sleep(1)  # car is parked for some period
        parked_time += 1
        if SHUTDOWN:
            return
    remove_reservation(client_id, start, end)


def process_requests():
    """Check for new reservations and if exist process them."""
    while True:
        try:
            client_id, start, end = REQUESTS.get_nowait()
        except queue.Empty:
            yield
            continue

        spot_id = get_available_spot_id(start, end)
        if spot_id is None:
            REJECTED_REQUESTS.append((client_id, start, end))
            yield
            continue

        yield spot_id, client_id, start, end


def make_random_reservations():
    """Make a new reservation every 5 seconds."""
    client_id = 0
    while not SHUTDOWN:
        client_id += 1
        start = randint(0, 23)
        end = randint(start + 1, 24)
        REQUESTS.put((client_id, start, end))
        sleep(5)


def read_client_from_file():
    """Read a client from user input."""
    pipe_path = "/tmp/mypipe"
    if not os.path.exists(pipe_path):
        os.mkfifo(pipe_path)
    pipe_fd = os.open(pipe_path, os.O_RDONLY | os.O_NONBLOCK)
    with os.fdopen(pipe_fd) as pipe:
        while True:
            data = pipe.readline()
            if not data:
                yield
                continue
            client_id, start, end = data.split()
            yield (client_id, int(start), int(end))


def run():
    try:
        request_handler = process_requests()
        client_reader = read_client_from_file()
        while True:
            client = next(client_reader)
            if client:
                REQUESTS.put(client)
            args = next(request_handler)
            if args:
                t = Thread(target=park_car, args=args)
                THREADS.append(t)
                t.start()
    except KeyboardInterrupt:
        request_handler.close()
        client_reader.close()


if __name__ == '__main__':
    THREADS.extend([
        Thread(target=print_parking),
        Thread(target=make_random_reservations)
    ])
    [t.start() for t in THREADS]
    run()

    print("\nWait for all threads to stop properly.")
    SHUTDOWN = True
    [t.join() for t in THREADS]
